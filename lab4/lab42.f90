! Lab 4, task 2

program task2
    implicit none
    integer :: N=5, display_iteration = 1
    real(kind=8) :: odd_sum

    call ComputeSum(N,display_iteration,odd_sum)

end program task2

subroutine computeSum(N,display_iteration,odd_sum)
   implicit none
   integer :: i1
   !integer, intent(in), bool, intent(in) :: N, display_iteration
   integer, intent(in) :: N
   integer, intent(in) :: display_iteration
   real(kind=8), intent(out) :: odd_sum

   odd_sum = 0.d0
   !Add a do-loop which sums the first N odd integers and prints the result

   do i1 = 1,N
      odd_sum = odd_sum + dble((2*i1)-1)
      if (display_iteration == 1) then
         print *,'Intermediate value = ', odd_sum
      end if
   end do

   print * , 'odd_sum = ', odd_sum


end subroutine ComputeSum

! To compile this code:
! $ gfortran -o task2.exe lab42.f90
! To run the resulting executable: $ ./task2.exe
