#Assumes lab5 fortran code has been compiled with: $ f2py -c lab5.f90 -m l5
import numpy as np
import matplotlib.pyplot as plt
from l53 import quad

#set parameters, initialze arrays
nvalues = [6,12,24,48]
I_mid = np.zeros(len(nvalues))
I_sim = np.zeros(len(nvalues))

#compute integrals with different resolutions
for i,n in enumerate(nvalues):
    # Compute integrals with n intervals with each method, storing the results
    # in I_mid and I_sim
    quad.quad_n = n
    quad.midpoint()
    I_mid[i] = quad.quad_sum
    quad.simpson()
    I_mid[i] = quad.quad_sum


print('*')
#Plot errors on log-log plots
#plt.figure()
#plt.loglog(I_mid, I_sim)
